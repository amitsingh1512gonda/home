package com.example.amit.home;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit on 1/27/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    //name of database for home==home.db
   public static final String DATABASE_NAME ="home.db";

    //User account table columns..........................

    public static final String TABLE_ACCOUNT ="account";
    public static final String COL_A1 ="ID";
    public static final String COL_A2 ="Username";
    public static final String COL_A3 ="Password";
    public static final String COL_A4 ="Name";
    public static final String COL_A5 ="Age";
    public static final String COL_A6 ="Email";

    //item name table.................

    public static final String TABLE_ITEMLIST ="itemlist";
    public static final String COL_I1 ="ID";
    public static final String COL_I2 ="Itemname";


//user items entries table...........
    public static final String TABLE_ENTRIES="entries";
    public static final String COL_E1 ="ID";
    public static final String COL_E2 ="Username";
    public static final String COL_E3 =COL_I2;
    public static final String COL_E4 ="Price";
    public static final String COL_E5 ="Date";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
     db.execSQL("CREATE TABLE "+ TABLE_ACCOUNT + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,Username TEXT,Password TEXT,Name TEXT,Age INTEGER,Email TEXT)");
     db.execSQL("CREATE TABLE "+ TABLE_ITEMLIST + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,Itemname TEXT)");
        db.execSQL("CREATE TABLE "+ TABLE_ENTRIES + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,Username TEXT ,Itemname TEXT,Price INTEGER,Date INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //clear data............
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRIES);

        //recreate the table
      onCreate(db);

    }
    //for account table insert..........

 SQLiteDatabase db = this.getWritableDatabase();
 public boolean insertData(String Username,String Password,String Name,String Age,String Email){
  ContentValues contentValues = new ContentValues();
  contentValues.put(COL_A2,Username);
  contentValues.put(COL_A3,Password);
  contentValues.put(COL_A4,Name);
  contentValues.put(COL_A5,Age);
  contentValues.put(COL_A6,Email);
  long result =db.insert(TABLE_ACCOUNT,null,contentValues);
     db.close();

  if (result==-1)
   return false;
  else
   return true;

 }


///username password search in accont table............

    public String search(String u,String p) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT Username, Password FROM " + TABLE_ACCOUNT + " WHERE Username=? AND Password=?", new String[]{u,p});
        String a, b;
        b="not found";

        if (cursor.moveToFirst())
        do {
            a = cursor.getString(0);
            if (a.equals(u))
              {
                b = cursor.getString(1);
                break;
              }
            }
while (cursor.moveToNext());
        return b;


    }

    SQLiteDatabase dbb = this.getWritableDatabase();
    public boolean insertData(String Itemname) {
        ContentValues c =new ContentValues();
        c.put(COL_I2, Itemname);
        long result =dbb.insert(TABLE_ITEMLIST, null, c);
        db.close();

        if (result==-1)
            return false;
        else
            return true;
    }

    public List<String> getAllLabels() {
        List<String> labels = new ArrayList<String>();
        // Select All Query......
        
        SQLiteDatabase dbi = this.getReadableDatabase();
       Cursor cursor = dbi.rawQuery("SELECT * FROM "+TABLE_ITEMLIST, null);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        dbi.close();

        // returning lables
        return labels;
    }



    public boolean entries(String u, String i, String price, String date) {
        SQLiteDatabase dbe=this.getWritableDatabase();
        ContentValues c=new ContentValues();
        c.put(COL_E2, u);
        c.put(COL_E3,i);
        c.put(COL_E4, price);
        c.put(COL_E5,date);
        long result =dbe.insert(TABLE_ENTRIES, null, c);
        db.close();

        if (result==-1)
            return false;
        else
            return true;
    }

    public Cursor viewentry() {
        SQLiteDatabase db =this.getWritableDatabase();
        Cursor res =db.rawQuery("select * from "+TABLE_ENTRIES ,null);
        return  res;
    }
}

