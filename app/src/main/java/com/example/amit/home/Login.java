package com.example.amit.home;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity implements View.OnClickListener {
    Button bLogin;
    EditText etUsername, etPassword;
    DatabaseHelper myldb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        bLogin = (Button) findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);
myldb=new DatabaseHelper(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.bLogin) {

                String u = etUsername.getText().toString();
                String p = etPassword.getText().toString();

                //method for text field validation......username
                if (u.matches("")) {
                    Toast.makeText(Login.this, "Username can not be empty", Toast.LENGTH_SHORT).show();return;
                }
                //for password field...........
                if (p.matches("")) {
                    Toast.makeText(Login.this, "Password can not be empty", Toast.LENGTH_SHORT).show();return;
                }

            //username password validation..............
            String password = myldb.search(u,p);
            if (p.equals(password)) {
                Intent intent=new Intent(Login.this,AddView.class);
//sending user name to AddView activity...............
                intent.putExtra("Username",u);
                startActivity(intent);


            }
            else {
                Toast.makeText(Login.this, "Username and password not match", Toast.LENGTH_SHORT).show();

            }

        }


    }
}


