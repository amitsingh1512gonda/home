package com.example.amit.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AddView extends AppCompatActivity implements View.OnClickListener {
    Button btnAdd,btnView;
    TextView tvuname;
    String u;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_view);
       btnAdd=(Button)findViewById(R.id.btnAdd);
        btnView=(Button)findViewById(R.id.btnView);
        tvuname=(TextView)findViewById(R.id.tvuname);
        btnAdd.setOnClickListener(this);
        btnView.setOnClickListener(this);
        //getting username.........
        Bundle extras=getIntent().getExtras();
      String u=extras.getString("Username");
        tvuname.setText(u);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnAdd){tvuname.getText().toString();
            Intent intent=new Intent(this,AddRecord.class);
            intent.putExtra("Username",u);
            startActivity(intent);
        }
       else if (v.getId()==R.id.btnView){
            Intent intent=new Intent(this,ViewRecord.class);
            intent.putExtra("Username",u);
            startActivity(intent);
        }
    }
}
