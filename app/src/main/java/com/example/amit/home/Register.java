package com.example.amit.home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity implements View.OnClickListener {
    DatabaseHelper myDb;
    Button bRegister;
    EditText etName, etAge, etrUsername, etrPassword, etrcPassword, etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //account databasehelper...........

        myDb = new DatabaseHelper(this);

        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etrUsername = (EditText) findViewById(R.id.etrUsername);
        etrPassword = (EditText) findViewById(R.id.etrPassword);
        etrcPassword = (EditText) findViewById(R.id.etrcPassword);
        bRegister = (Button) findViewById(R.id.bRegister);
        bRegister.setOnClickListener(this);

    }

    //inserting data in register table..............
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bRegister) {
            String n = etName.getText().toString();
            String a = etAge.getText().toString();
            String e = etEmail.getText().toString();
            String u = etrUsername.getText().toString();
            String p = etrPassword.getText().toString();
            //name field validation...........
            if (n.matches("")) {
                Toast.makeText(Register.this, "name can not be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            //age..........

            if (a.matches("")) {
                Toast.makeText(Register.this, "Age can not be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            ///email.........

            if (e.matches("")) {
                Toast.makeText(Register.this, "Email can not be empty", Toast.LENGTH_SHORT).show();
                return;
            }

            //password field validation.........
            if (u.matches("")) {
                Toast.makeText(Register.this, "Username can not be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            //password field validation............
            if (p.matches("")) {
                Toast.makeText(Register.this, "Password can not be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            //password verification match..........
            if (!p.equals(etrcPassword.getText().toString())) {
                //popup msg password not match
                Toast.makeText(Register.this, "password not match", Toast.LENGTH_SHORT).show();
                return;

            }

            //data insertion in register table..........
            boolean insertData = myDb.insertData( etrUsername.getText().toString(), etrPassword.getText().toString(),etName.getText().toString(), etAge.getText().toString(), etEmail.getText().toString());


            //sucessful registration message.........
            if (insertData = true) {
                Toast.makeText(Register.this, "Registered successfully", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(Register.this, "not registered", Toast.LENGTH_SHORT).show();

            }

        }
    }
}