package com.example.amit.home;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class AddRecord extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    Spinner Items_spinner;
    EditText etAddlist, etPrice;
    Button btnAddlist,btnsubmit;
   String i;
    DatabaseHelper addDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_record);
        Items_spinner = (Spinner) findViewById(R.id.Items_spinner);
        etAddlist = (EditText) findViewById(R.id.etAddlist);
        etPrice = (EditText) findViewById(R.id.etPrice);
        btnAddlist = (Button) findViewById(R.id.btnAddlist);
        btnsubmit = (Button) findViewById(R.id.btnsubmit);
        btnsubmit.setOnClickListener(this);
        btnAddlist.setOnClickListener(this);
        //getting the string username from intent i



//creating spinner
        Spinner spinner = (Spinner) findViewById(R.id.Items_spinner);
        DatabaseHelper sDb = new DatabaseHelper(getApplicationContext());
        //Spinnr drop down elements....
        List<String> lables = sDb.getAllLabels();
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddRecord.this, android.R.layout.simple_spinner_item, lables);

//layout to use when the list of choices appears.......
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Select Items From List");
        spinner.setOnItemSelectedListener(this);

    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String i=  parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onClick(View v) {

        Bundle extras=getIntent().getExtras();
        String u=extras.getString("Username");
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String price=etPrice.getText().toString();
        if (v.getId()==R.id.btnsubmit) {
            addDb=new DatabaseHelper(this);
            boolean  entries=addDb.entries(u,i,price,date);
            if (entries = true) {
                Toast.makeText(AddRecord.this, "record Added successfully", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddRecord.this, "Not Added", Toast.LENGTH_SHORT).show();

            }


        }
        else  if (v.getId()==R.id.btnAddlist) {
            //databse helper for add record....
            addDb =new DatabaseHelper(this);
            String addlist =etAddlist.getText().toString();

            //list insertion in Itemlist table..........
            boolean insertData =addDb.insertData(etAddlist.getText().toString());
            if (insertData = true) {
                Toast.makeText(AddRecord.this, "Item Added successfully", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(AddRecord.this, "Not Added", Toast.LENGTH_SHORT).show();

            }

        }

    }


}
