package com.example.amit.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.widget.Button;

public class Home extends AppCompatActivity implements View.OnClickListener{
    Button hLogin;
    Button hRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        hLogin = (Button)findViewById(R.id.hLogin);
        hRegister = (Button)findViewById(R.id.hRegister);
        hLogin.setOnClickListener(this);
        hRegister.setOnClickListener(this);




    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.hLogin:
                startActivity(new Intent(this,Login.class));
break;
            case R.id.hRegister:
                startActivity(new Intent(this,Register.class));
                break;
        }

    }
}
