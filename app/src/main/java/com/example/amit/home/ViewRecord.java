package com.example.amit.home;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ViewRecord extends AppCompatActivity implements View.OnClickListener {
    String u;
   Button btnViewrecord;
    DatabaseHelper entry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_record);
        Bundle extras=getIntent().getExtras();
        String u=extras.getString("Username");
        btnViewrecord=(Button)findViewById(R.id.btnViewrecord);
        btnViewrecord.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        entry =new DatabaseHelper(this);
        Cursor res=entry.viewentry();
if (res.getCount()==0){
    //show message  nothing in record...
Showmessage("Error","Nothing Found");
    return;
}
          StringBuffer buffer=new StringBuffer();
        while (res.moveToNext()){
            buffer.append("ID :"+res.getString(0)+"\n");
            buffer.append("Username:"+res.getString(1)+"\n");
            buffer.append("Itemname:"+res.getString(2)+"\n");
            buffer.append("Price:"+res.getString(3)+"\n");
            buffer.append("Date:"+res.getString(4)+"\n");
            //show all entries
            Showmessage("Data", buffer.toString());
        }

    }



    private void Showmessage(String error, String s) {
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(error);
        builder.setMessage(s);
        builder.show();
    }
}
